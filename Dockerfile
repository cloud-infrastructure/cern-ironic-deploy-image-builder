FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest

# Get the latest aims2client from linux team development. The dirty way.
RUN yum-config-manager --add-repo http://linuxsoft.cern.ch/internal/repos/linuxsupport7-stable/x86_64/os && yum clean all && yum -y install aims2-client --disableplugin=* --nogpgcheck

RUN yum -y install qemu sudo kpartx cpio python-pip git e4fsprogs vim policycoreutils-python tree
RUN pip install diskimage-builder==2.33.0

RUN mkdir -p /root/.ssh/
COPY authorized_keys /root/.ssh/authorized_keys

RUN mkdir -p /ironic-image-creation/
WORKDIR /ironic-image-creation
RUN git clone https://gitlab.cern.ch/cloud-infrastructure/cci-elements.git

COPY create_image.sh /ironic-image-creation/create_image.sh
COPY upload_aims_image.sh /ironic-image-creation/upload_aims_image.sh
