#!/bin/bash -ex
COMMIT_ID=$(git ls-remote https://:@gitlab.cern.ch:8443/cloud-infrastructure/ironic-python-agent.git refs/heads/$IPA_BRANCH | cut -c1-8)

# aims2client remimage $AIMS_NAME
if [ -z $AIMS_NAME ] ; then
    RELEASE_NAME=$(echo $IPA_BRANCH | sed 's|.*/||')
    AIMS_NAME="OPENSTACK-IPA-${RELEASE_NAME}-${COMMIT_ID}-${HW_BRANCH}"
fi

aims2client delimg $AIMS_NAME || true # delete image if it exist
aims2client addimage --name $AIMS_NAME --arch x86_64 --description="OPENSTACK IPA $COMMIT_ID CERN-HW-MANAGER $HW_BRANCH" --vmlinuz /ironic-image-creation/outputimage/$TEMP_FOLDER/$TEMP_FOLDER.kernel --initrd /ironic-image-creation/outputimage/$TEMP_FOLDER/$TEMP_FOLDER.initramfs
