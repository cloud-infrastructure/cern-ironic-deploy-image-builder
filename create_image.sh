#!/bin/bash -ex
# OPTIONAL PARAMETERS FOR CREATING A DEBUG IMAGE
# DIB_DEV_USER_PWDLESS_SUDO=yes DIB_DEV_USER_USERNAME=devuser DIB_DEV_USER_PASSWORD=$IRONIC_PASSWORD

mkdir -p /ironic-image-creation/outputimage/$TEMP_FOLDER

sed -i "s/HWMANAGER_BRANCH=master/HWMANAGER_BRANCH=$HW_BRANCH/" cci-elements/ironic-hwmanager-cern/install.d/ironic-hwmanager-cern-downloader

ELEMENTS_PATH=cci-elements DIB_REPOREF_ironic_agent=$IPA_BRANCH disk-image-create -o /ironic-image-creation/outputimage/$TEMP_FOLDER/$TEMP_FOLDER centos7 posix local-config disable-selinux cleanup-kernel-initrd ironic-agent ironic-hwmanager-cern ironic-rsyslog mellanox docker-ce
