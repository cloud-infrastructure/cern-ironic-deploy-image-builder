# cern-deploy-ironic-image-builder

To create the container image:

```
docker build -t cern-ironic-deploy-image-builder:latest .
```

To run the container where the actual image will be built execute the following commands. The parameters are self-explanatory. You might want to modify the IPA_BRANCH, if you are trying to test code in the ironic-python-agent (which by default points to https://gitlab.cern.ch/cloud-infrastructure/ironic-python-agent) and the AIMS_NAME, which is the name with which the image will be uploaded to AIMS.

```
IPA_BRANCH=cern/rocky
HW_BRANCH=master
TEMP_FOLDER=ironic_$(date +"%Y%m%d_%H%M")
AIMS_NAME='OPENSTACK-IRONIC-DEPLOY-DELETEME'
docker run -v /dev:/dev --privileged --mount source=images,target=/ironic-image-creation/outputimage -e "HW_BRANCH=$HW_BRANCH" -e "IPA_BRANCH=$IPA_BRANCH" -e "TEMP_FOLDER=$TEMP_FOLDER" -e "AIMS_NAME=$AIMS_NAME" -it cern-ironic-deploy-image-builder:latest /bin/bash
```

```
./create_image.sh # For creating the image
./upload_aims_image.sh # In order to upload the image to AIMS
```

~~NOTE: the authorized_keys file is kept outside of git (even if it just contains the public keys that will be copied to the deploy_image for debugging)~~
It was decided to keep pub keys in the repo to allow GitLab CI to build containers.

#### Building a new image with user credentials

Sometimes, nodes do not get network connectivity, so ssh is not an option for debugging. That's when having username and password might come in handy.

To build an image, run the same container as mentioned above. One just needs to modify the `disk-image-create` command to include the [devuser](https://docs.openstack.org/diskimage-builder/latest/elements/devuser/README.html) element and modify the variables (username and password) accordingly, resulting in a command like:

```
DIB_DEV_USER_PWDLESS_SUDO=yes DIB_DEV_USER_USERNAME=devuser DIB_DEV_USER_PASSWORD=$IRONIC_PASSWORD ELEMENTS_PATH=cci-elements DIB_REPOREF_ironic_agent=$IPA_BRANCH disk-image-create -o /ironic-image-creation/outputimage/$TEMP_FOLDER/$TEMP_FOLDER centos7 posix local-config disable-selinux cleanup-kernel-initrd ironic-agent ironic-hwmanager-cern ironic-rsyslog mellanox devuser
```

To get the password:
```
tbag show --hg cloud_baremetal/all_in_one ironic_deploy_image_devuser_password
```

# Issues

https://bugs.launchpad.net/diskimage-builder/+bug/1848062

#### Building, tagging and uploading docker image to GitLab Container Registry

This repository has "Container Registry" enabled (click on "Packages" -> "Container Registry"). By executing the commands below we can version docker images and upload them into gitlab for safe storage. 

```bash 
# this will build image and tag it to be both the `latest` and to commit number.
# --label can be used to add metadata with useful info  
docker build --label "aims2-client-2.28-1.el7.cern, with increased upload size limit" \
	-t "gitlab-registry.cern.ch/cloud-infrastructure/cern-ironic-deploy-image-builder:c-$(git rev-list --count HEAD)" \
	-t gitlab-registry.cern.ch/cloud-infrastructure/cern-ironic-deploy-image-builder:latest .
docker login gitlab.cern.ch  # loggin with your CERN credentials
docker push "gitlab-registry.cern.ch/cloud-infrastructure/cern-ironic-deploy-image-builder:c-$(git rev-list --count HEAD)"
docker push gitlab-registry.cern.ch/cloud-infrastructure/cern-ironic-deploy-image-builder:latest
```


